
/*  In this file: Creating a Router!  */


// necessary imports
import Vue from 'vue'
import VueRouter from 'vue-router'


// import all my pages / views for the navbar
import Home from '@/views/Home.vue'
import Gallery from '@/views/Gallery.vue'
import Friends from '@/views/Friends.vue'
import NotFound from '@/views/NotFound.vue'

// not sure what this does exactly
Vue.use(VueRouter)

// associating routes with components
const routes = [
	{
		name: "Home",
		path: '/',
		component: Home
	},
	{
		name: "Gallery",
		path: '/gallery',
		component: Gallery
	},
	{
		name: "Friends",
		path: '/friends',
		component: Friends
	},
	{
		name: "404 Landing Page",
		path: '/*',
		component: NotFound
	}
	
]

// creating new VueRouter instance and pass in the above declared routes
const router = new VueRouter({
	routes: routes,
	mode: 'history',
	// not sure what this on exactly does:
	base: process.env.BASE_URL
})

// export for use in main.js where it gets injected into the Vue instance
export default router