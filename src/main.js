import Vue from 'vue'
import App from './App.vue'

// importing the router I created in @/router/index.js
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,     // injecting router here. Now router-view and router-link are useable in the Navbar
  render: h => h(App),
}).$mount('#app')