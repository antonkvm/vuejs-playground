# My Vue Project

## (Currently not functional, outdated and conflicting dependencies)

## Generic Structure
- `App.vue` is bascially the entry point.
- It imports components:
	- Navbar
	- Footer
- Between those, a `<router-view>` element renders the main content.
- The `/src/main.js` File creates a Vue instance, imports `App.vue`, and injects the rendered content into `/public/index.html`.

## How the Navbar works
- The Navbar component receives a 'pages' array of objects as a prop from `App.vue`.
- Each object contains the name of a page and it's path as defined in the vue router.
- In `TheNavbar.vue`, a `v-for loop` creates a nav item for each object in the pages array.
- These nav items contain `<router-link>` elements that point to the respective path.
- The `activeRoute` computed propery keeps track of the currently active URL by accessing the `$route` object.
- The active `<router-link>` element (aka the page we're currently looking at) gets the 'active' class if its target path matches `activeRoute`

## The VueRouter
- Gets defined and created at `/src/router/index.html`.
- Gets imported by `/src/main.js` and injected into the Vue instance.
- Defines routes that map specific paths to specific components. If the URL matches a route, the `<router-view>` element renders that component.

## Directory structure (where does what file go?)
- `App.vue` lies in `/src/`.
- single-use components (e.g. Navbar, Footer) go in `/src/components/`.
- content components (aka <main> elements) go in the `/src/views` folder.
- Other assets like fonts, images, etc. go in the `/src/assets/` folder.
	- Accessing that directory is still a bit weird to me, so as an alternative, use `/public/`.

## Git and Dist (OBSOLETE, MOVED TO CLOUDFLARE PAGES)
- The dist directory has been removed from `.gitignore` and gets pushed with every commit.
- The raspberrypi (the webserver) pulls the git repo, including the dist, every 60 seconds via crontab.
- So, in order to push a new production, just run `npm run build` and push the changes to the repo. Then wait 5 minutes at most.

## Notes to self
- `<router-link>` tags apparently cannot fire @click events. Solve this by putting the @click tag in a div wrapper or parent element, if possible.
- test


# Stuff genereated by VueCLI:

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
